#pragma once

// Project
#include "settings/settings.h"

// STL

// Native

// Qt
#include <QDialog>

namespace Ui {
class DialogSvnManager;
}

class DialogSvnManager : public QDialog {
  Q_OBJECT

 private:
  SvnManager &m_svnManager;
  Ui::DialogSvnManager *ui;

 signals:
  void onSelected();

 public:
  explicit DialogSvnManager(QWidget *parent = 0);
  ~DialogSvnManager();

 private:
  void updateSize();
  void loadListSvn();
  void saveListSvn();
  void addUrl(const QString &url);
  void setLabelCurrentUrl();

 private slots:
  void on_pushButton_add_clicked();
  void on_pushButton_del_clicked();
  void on_pushButton_def_clicked();
  void on_pushButton_select_clicked();
  void on_pushButton_edit_clicked();
};
