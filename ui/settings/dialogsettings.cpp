// Project
#include "dialogsettings.h"
#include "settings/settings.h"
#include "ui_dialogsettings.h"

// STL

// Native

// Qt
#include <QFileDialog>

DialogSettings::DialogSettings(QWidget *parent)
    : QDialog(parent), ui(new Ui::DialogSettings) {
  ui->setupUi(this);
  setWindowFlags(windowFlags() & ~Qt::WindowContextHelpButtonHint);

  connect(ui->horizontalSlider_appFontSize, SIGNAL(valueChanged(int)),
          Settings::get(), SLOT(saveAppFontSize(int)));
  connect(ui->lineEdit_pathPackages, &QLineEdit::textChanged, Settings::get(),
          &Settings::setPathPackages);
  connect(ui->checkBox_enableAutoupdate, &QCheckBox::toggled, Settings::get(),
          &Settings::setStateAutoupdate);

  ui->horizontalSlider_appFontSize->setValue(Settings::get()->getAppFontSize());
  ui->lineEdit_pathPackages->setText(Settings::get()->getPathPackages());
  ui->checkBox_enableAutoupdate->setChecked(
      Settings::get()->getStateAutoupdate());

  resize(sizeHint());
}

DialogSettings::~DialogSettings() { delete ui; }

void DialogSettings::on_toolButton_resetPathPackages_clicked() {
  Settings::get()->resetPathPackages();
  ui->lineEdit_pathPackages->setText(Settings::get()->getPathPackages());
}

void DialogSettings::on_toolButton_selectPathPackages_clicked() {
  QString path = ui->lineEdit_pathPackages->text();
  QString dir = QFileDialog::getExistingDirectory(
      this, trUtf8("Выбор директории"), path,
      QFileDialog::ShowDirsOnly | QFileDialog::DontResolveSymlinks);

  if (!dir.isEmpty()) {
    ui->lineEdit_pathPackages->setText(dir);
    Settings::get()->setPathPackages(dir);
  }
}
