// Project
#include "autoupdate.h"

// STL

// Native

// Qt
#include <QApplication>
#include <QCryptographicHash>
#include <QFile>
#include <QFileInfo>
#include <QMessageBox>

Autoupdate::Autoupdate(QObject *parent) : QObject(parent) {
  m_manager = new QNetworkAccessManager(this);
  m_urlVersion = "http://hiparadox.ru/hisvn2/update/version";
  m_urlProgram = "http://hiparadox.ru/hisvn2/update/HiSvn2.exe";

  m_timerTimeout.setInterval(m_timeout);
  connect(&m_timerTimeout, &QTimer::timeout, this, &Autoupdate::timeout);

  initDownloadProgressDialog();
}

void Autoupdate::updateInterrupted(const QString &str, const QString str2) {
  LOG(WARNING) << trUtf8("Обновление прервано.");

  if (str2.isEmpty())
    LOG(WARNING) << trUtf8("Причина: %1").arg(str);
  else {
    LOG(WARNING) << trUtf8("Не удалось удалить временный файл «%1».").arg(str);
    LOG(WARNING) << trUtf8("Причина: %1").arg(str2);
  }
}

void Autoupdate::initDownloadProgressDialog() {
  m_downloadProgressDialog = new QProgressDialog(
      0, Qt::Window | Qt::WindowTitleHint | Qt::CustomizeWindowHint);
  m_downloadProgressDialog->setModal(true);
  m_downloadProgressDialog->setLabelText(
      trUtf8("Загрузка обновления, подождите..."));
  m_downloadProgressDialog->setWindowTitle(trUtf8("Прогресс загрузки"));
  m_downloadProgressDialog->setCancelButtonText(trUtf8("Отменить загрузку"));
  m_downloadProgressDialog->setFixedSize(m_downloadProgressDialog->sizeHint());
  m_downloadProgressDialog->reset();
  connect(m_downloadProgressDialog, &QProgressDialog::canceled,
          m_downloadProgressDialog, &QProgressDialog::close);
}

void Autoupdate::timeout() {
  m_timerTimeout.stop();
  m_reply->abort();
}

void Autoupdate::readFileFinished() {
  if (m_reply == nullptr) return;
  QString errorString = m_reply->errorString();
  QNetworkReply::NetworkError error = m_reply->error();

  QByteArray replyData;
  if (error == QNetworkReply::NoError) replyData = m_reply->readAll();

  m_reply->deleteLater();
  m_reply = nullptr;

  if (m_timerTimeout.isActive()) {
    m_timerTimeout.stop();
  } else {
    error = QNetworkReply::TimeoutError;
    errorString = trUtf8("Время на соединение истекло");
  }

  if (error == QNetworkReply::NoError) {
    processingUpdate(replyData);
  } else {
    updateInterrupted(errorString);

    m_downloadProgressDialog->close();
  }
}

void Autoupdate::processingUpdate(QByteArray data) {
  LOG(INFO) << trUtf8("Устанавливаем обновление приложения.");

  int size = data.size();
  QString hash = QCryptographicHash::hash(data, QCryptographicHash::Sha3_512)
                     .toHex()
                     .toUpper();

  if (m_newFileSize != size || m_newFileHash != hash) {
    updateInterrupted(trUtf8("Cкачанный файл обновления повреждён!"));
    return;
  }

  QString appFilePath = QApplication::applicationFilePath();
  QString appDirPath = QApplication::applicationDirPath();
  QFileInfo appFileInfo(appFilePath);
  QString curAppFileName = appFileInfo.fileName();
  QString newAppFileName = "new_" + curAppFileName;
  QString delAppFileName = QString("delete_file_%1").arg(curAppFileName);

  //Удаляем предыдущий файл приложения
  QFile delAppFile(appDirPath + QDir::separator() + delAppFileName);
  if (delAppFile.exists() && !delAppFile.remove()) {
    updateInterrupted(
        trUtf8("Не удалось удалить временный файл «%1».").arg(delAppFileName),
        delAppFile.errorString());
    return;
  }

  //Сохранение обновления
  QFile newFileUpdate(appDirPath + QDir::separator() + newAppFileName);
  if (newFileUpdate.open(QIODevice::WriteOnly)) {
    newFileUpdate.write(data);
    newFileUpdate.close();
  } else {
    updateInterrupted(
        trUtf8("Не удалось открыть файл «%1».").arg(newAppFileName),
        newFileUpdate.errorString());
    return;
  }

  //Переименование файла основного приложения во временный файл
  QFile fileApp(appFilePath);
  if (!fileApp.rename(appDirPath + QDir::separator() + delAppFileName)) {
    updateInterrupted(
        trUtf8("Не удалось переименовать файл основного приложения."),
        fileApp.errorString());
    return;
  }

  //Переименование файла обновления
  QFile newfileApp(appDirPath + QDir::separator() + newAppFileName);
  if (!newfileApp.rename(appDirPath + QDir::separator() + curAppFileName)) {
    updateInterrupted(trUtf8("Не удалось переименовать файл обновления."),
                      newfileApp.errorString());
    return;
  }

  //Запуск обновлённого приложения
  if (!QProcess::startDetached("\"" + appFilePath + "\"")) {
    updateInterrupted(trUtf8("Не удалось запустить обновлённое приложение."));
    return;
  }

  LOG(INFO) << trUtf8("Обновление успешно установлено.");
  LOG(INFO) << trUtf8("Перезапускаем приложение.");
  qApp->exit();
}

void Autoupdate::downloadProgress(qint64 bytesRead, qint64 totalBytes) {
  if (m_reply->error() == QNetworkReply::NoError) m_timerTimeout.start();

  m_downloadProgressDialog->setMaximum(totalBytes);
  m_downloadProgressDialog->setValue(bytesRead);
}

void Autoupdate::goUpdate() {
  LOG(INFO) << trUtf8("Загружаем обновление...");

  m_downloadProgressDialog->setFixedSize(m_downloadProgressDialog->sizeHint());
  m_downloadProgressDialog->reset();
  m_downloadProgressDialog->show();

  m_timerTimeout.start();
  m_reply = m_manager->get(QNetworkRequest(m_urlProgram));
  connect(m_reply, &QNetworkReply::finished, this,
          &Autoupdate::readFileFinished);
  connect(m_reply, &QNetworkReply::downloadProgress, this,
          &Autoupdate::downloadProgress);
  connect(m_downloadProgressDialog, &QProgressDialog::canceled, m_reply,
          &QNetworkReply::abort);
}

void Autoupdate::processingVersion() {
  QString updateInfo;
  double sizeKb = m_newFileSize / 1024.0 / 1024.0;

  if (qApp->applicationVersion() != m_newVersion) {
    updateInfo = trUtf8(
                     "Доступна новая версия.\nРазмер обновления: %1 "
                     "МБ.\nОбновиться до версии %2?")
                     .arg(QString::number(sizeKb, 'f', 1))
                     .arg(m_newVersion);
    LOG(INFO) << trUtf8("Доступна новая версия: %1").arg(m_newVersion);
    LOG(INFO) << trUtf8("Размер обновления: %1 МБ")
                     .arg(QString::number(sizeKb, 'f', 1));

    int reply =
        QMessageBox::question(0, trUtf8("Доступно обновление"), updateInfo);
    if (QMessageBox::Yes == reply) {
      goUpdate();
    }
  } else {
    if (m_silent) return;

    QString noUp = trUtf8("Обновление не требуется.");
    LOG(INFO) << noUp;
    QMessageBox::information(0, trUtf8("Обновление"), noUp);
  }
}

bool Autoupdate::parseVersion(QByteArray data) {
  QJsonParseError parseError;
  QJsonDocument jsonDoc = QJsonDocument::fromJson(data, &parseError);
  if (parseError.error != QJsonParseError::NoError) return false;

  QJsonObject objects = jsonDoc.object();
  m_newVersion = objects["version"].toString();
  m_newFileSize = objects["size"].toInt();
  m_newFileHash = objects["hash"].toString();

  return true;
}

void Autoupdate::readVersionFinished() {
  if (m_reply == nullptr) return;
  QString errorString = m_reply->errorString();
  QNetworkReply::NetworkError error = m_reply->error();
  QByteArray replyData = m_reply->readAll();
  m_reply->deleteLater();
  m_reply = nullptr;

  if (m_timerTimeout.isActive()) {
    m_timerTimeout.stop();
  } else {
    errorString = trUtf8("Время на соединение истекло");
  }

  if (error == QNetworkReply::NoError) {
    if (parseVersion(replyData)) {
      processingVersion();
      return;
    } else {
      errorString = trUtf8("Не удалось разобрать ответ сервера.");
    }
  }

  if (!m_silent) updateInterrupted(errorString);
}

void Autoupdate::checkUpdate(bool silent) {
  if (m_timerTimeout.isActive()) {
    LOG(WARNING) << trUtf8("Проверка обновления уже запущена!");
    return;
  }
  m_silent = silent;

  if (!m_silent) LOG(INFO) << trUtf8("Проверяем обновление приложения.");

  m_timerTimeout.start();
  m_reply = m_manager->get(QNetworkRequest(m_urlVersion));
  connect(m_reply, &QNetworkReply::finished, this,
          &Autoupdate::readVersionFinished);
}
