QT += core gui network widgets
TARGET = HiSvn2
TEMPLATE = app
CONFIG += c++14

#Информация о приложении
QMAKE_TARGET_COMPANY = "HiAsm community(c)"
QMAKE_TARGET_DESCRIPTION = "Updating elements HiAsm"
QMAKE_TARGET_COPYRIGHT = "CriDos"
QMAKE_TARGET_PRODUCT = "HiSvn"
RC_ICONS = "res/icon/icon.ico"
RC_LANG = "0x419"
VERSION = "2.2.0.5" #Версия приложения, ваш Кэп:)

DEFINES += APP_COMPANY=\"\\\"$$QMAKE_TARGET_COMPANY\\\"\" \
           APP_DESCRIPTION=\"\\\"$$QMAKE_TARGET_DESCRIPTION\\\"\" \
           APP_COPYRIGHT=\"\\\"$$QMAKE_TARGET_COPYRIGHT\\\"\" \
           APP_PRODUCT=\"\\\"$$QMAKE_TARGET_PRODUCT\\\"\" \
           APP_VERSION=\"\\\"$$VERSION\\\"\"

win32-msvc*{
    #Поддержка Windows XP
    contains(QT_ARCH, i386) {
        QMAKE_LFLAGS_WINDOWS = /SUBSYSTEM:WINDOWS,5.01
    } else {
        QMAKE_LFLAGS_WINDOWS = /SUBSYSTEM:WINDOWS,5.02
    }
}

#Манифест
win32-msvc*{
    #CONFIG += embed_manifest_exe
    #Запрос прав администратора
    #QMAKE_LFLAGS += " /MANIFESTUAC:level=\'requireAdministrator\'"
}

include("3rd/qtsingleapplication/qtsingleapplication.pri")

#Подключаем логгер
include("3rd/easyloggingpp/logger.pri")

#Подключаем обновлятор
include("3rd/autoupdate/autoupdate.pri")

#Основные файлы проекта
INCLUDEPATH += ui/changelog \
               ui/mainwindow \
               ui/svnmanager \
               ui/settings \
               ui/makeandbuild \
               util \
               settings

SOURCES += main.cpp
SOURCES += $$files("ui\*.cpp", true)
SOURCES += $$files("settings\*.cpp", true)
SOURCES += $$files("util\*.cpp", true)

HEADERS += $$files("ui\*.h", true)
HEADERS += $$files("settings\*.h", true)
HEADERS += $$files("util\*.h", true)

FORMS += $$files("ui\*.ui", true)

RESOURCES += res/resources.qrc
