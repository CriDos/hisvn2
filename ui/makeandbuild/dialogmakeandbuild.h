#pragma once

// Project

// STL

// Native

// Qt
#include <QDialog>
#include <QProcess>

class StatusWidget;

namespace Ui {
class DialogMakeAndBuild;
}

class DialogMakeAndBuild : public QDialog {
  Q_OBJECT

 private:
  QTextCodec *m_encoderCmd{};
  Ui::DialogMakeAndBuild *ui{};
  QProcess *m_process{};
  StatusWidget *m_currentStatusWidget{};
  QString m_command;
  bool m_disableExit = false;
  QStringList m_packagesList;

 public:
  explicit DialogMakeAndBuild(QWidget *parent = 0);
  ~DialogMakeAndBuild();

 private slots:
  void disableActionUi(bool state);
  bool isDisableExit() const;
  void receivProcessOutput();
  void receivProcessErrorOutput();
  void processFinished(int resultCode, QProcess::ExitStatus status);
  void onPackClicked();
  void on_pushButton_run_clicked();
  void on_pushButton_allChecked_clicked();
  void on_pushButton_allUncheck_clicked();

 private:
  QList<QPair<QString, QString>> getBatFiles();
  void initPackagesList();
  void addTableData();
  void addButtonsPack();
  void nextExecution();

  // QWidget interface
 protected:
  void closeEvent(QCloseEvent *event) override;
};
