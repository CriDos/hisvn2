#pragma once
// Project
#include "settings/settings.h"

// STL

// Native

// Qt
#include <QBuffer>
#include <QObject>
#include <QProcess>

QT_BEGIN_NAMESPACE
class QTextCodec;
QT_END_NAMESPACE

namespace SvnParams {
Q_CONSTEXPR char SVN_UTIL[] = "SVN/hisvn.exe";
}

namespace SvnCommands {
Q_CONSTEXPR char STATUS[] = "status --xml";
Q_CONSTEXPR char INFO[] = "info --xml";
Q_CONSTEXPR char UPDATE[] = "update --accept theirs-full --force";
Q_CONSTEXPR char UPGRADE[] = "upgrade";
Q_CONSTEXPR char CHECKOUT[] = "checkout";
Q_CONSTEXPR char CLEANUP[] = "cleanup";
Q_CONSTEXPR char REVERT[] = "revert -q -R";
Q_CONSTEXPR char RELOCATE[] = "relocate";
}

namespace NamesObjects {
Q_CONSTEXPR char STATUS_WORKING_COPY[] = "StatusWorkingCopy";
Q_CONSTEXPR char INFO_WORKING_COPY[] = "InfoWorkingCopy";
Q_CONSTEXPR char INFO_REPOSITORY[] = "InfoRepository";
Q_CONSTEXPR char UPDATE[] = "Update";
Q_CONSTEXPR char UPGRADE[] = "Upgrade";
Q_CONSTEXPR char CHECKOUT[] = "Checkout";
Q_CONSTEXPR char CLEANUP[] = "Cleanup";
Q_CONSTEXPR char REVERT[] = "Revert";
Q_CONSTEXPR char RELOCATE[] = "Relocate";
}

enum class TypesAction {
  null,
  undefined,
  success,
  getStatusWorkingCopy,
  getInfoWorkingCopy,
  getInfoRepository,
  update,
  upgrade,
  checkout,
  cleanup,
  revert,
  relocate
};

class SvnUtil : public QObject {
  Q_OBJECT

 private:
  const SvnManager &m_svnManager;
  QTextCodec *m_encoder;
  QProcess *m_workingCopy;
  QProcess *m_repository;
  QByteArray m_bufferWorkingCopyOutput;
  QByteArray m_bufferWorkingCopyError;
  QByteArray m_bufferRepositoryError;

 signals:
  void onRequiredAction(TypesAction);
  void onWorkingCopyRev(qint32);
  void onRepositoryRev(qint32);
  void onStateChanged(QProcess::ProcessState);

 public:
  explicit SvnUtil(QObject *parent = 0);

 private:
  QStringList arrayToStringList(const QByteArray &data);
  QString getActionName(const QString &str);
  qint32 getRevisionFromXML(const QByteArray &data);
  TypesAction getActionFromError(const QByteArray &data);
  void actionFromStatus(const QByteArray &data);

 private slots:
  void receivProcessOutput();
  void receivProcessErrorOutput();
  void receivProcessError(QProcess::ProcessError error);
  void finishedProcess(int exitCode, QProcess::ExitStatus exitStatus);

 public:
  void forcedStop();
  void executeAction(TypesAction action);
  void getInfoWorkingCopy();
  void getInfoRepository();
  void update();
  void upgrade();
  void checkout();
  void cleanup();
  void revert();
  void relocate();
  void deleteSvnDB();
  QProcess::ProcessState stateRunningProcess();
};
