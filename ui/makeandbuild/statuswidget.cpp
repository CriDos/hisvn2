// Project
#include "statuswidget.h"
#include "dialogviewlog.h"
#include "statustablewidget.h"
#include "ui_statuswidget.h"

// STL

// Native

// Qt

StatusWidget::StatusWidget() : ui(new Ui::StatusWidget) {
  ui->setupUi(this);

  m_waitPixmap.load(":/tableWidget/wait.png");
  m_executingPixmap.load(":/tableWidget/executing.png");
  m_warningPixmap.load(":/tableWidget/warning.png");
  m_okPixmap.load(":/tableWidget/ok.png");

  setWait();
}

StatusWidget::~StatusWidget() { delete ui; }

void StatusWidget::setWait() {
  ui->status->setPixmap(m_waitPixmap);
  ui->toolButton_log->setDisabled(true);
}

void StatusWidget::setExecuting() {
  ui->status->setPixmap(m_executingPixmap);
  ui->toolButton_log->setDisabled(true);
}

void StatusWidget::setWarning() {
  ui->status->setPixmap(m_warningPixmap);
  ui->toolButton_log->setDisabled(false);
}

void StatusWidget::setOk() {
  ui->status->setPixmap(m_okPixmap);
  ui->toolButton_log->setDisabled(false);
}

void StatusWidget::addErrorText(QString str) {
  m_noError = false;
  static const QString redText = "<font color='Red'>%1<\\font>";
  m_resultText += redText.arg(str.replace("\r", "<br>"));
  ;
}

void StatusWidget::addOutputText(QString str) {
  static const QString blackText = "<font color='Black'>%1<\\font>";

  m_resultText += blackText.arg(str.replace("\r", "<br>"));
}

void StatusWidget::setCommandText(const QString &str) { m_commandText = str; }

bool StatusWidget::emtyError() { return m_noError; }

void StatusWidget::reset() {
  m_noError = true;
  m_resultText.clear();

  setWait();
}

void StatusWidget::on_toolButton_log_clicked() {
  DialogViewLog viewLog(m_resultText, m_commandText);
  viewLog.exec();
}
