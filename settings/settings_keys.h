#pragma once

#include <QString>

//Ключи нактроек
namespace Keys {
namespace App {
extern const QString FONT_SIZE;
}
namespace Ui {
extern const QString WINDOW_GEOMETRY;
}
namespace SVNManager {
extern const QString URL_LIST;
extern const QString CURRENT_URL;
extern const QString PATH_PACKAGES;
}
namespace MakeAndBuild {
extern const QString EXCLUDE_DIRS;
extern const QString FIND_BAT_FILES;
extern const QString EXCLUDE_PACK_DIRS;
}
namespace Autoupdate {
extern const QString ENABLE_AUTOUPDATE;
}
}
