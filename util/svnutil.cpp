// Project
#include "svnutil.h"

// LOG
#include "logger.h"

// STL

// Native

// Qt
#include <QApplication>
#include <QDir>
#include <QProcess>
#include <QTextCodec>
#include <QXmlStreamReader>

SvnUtil::SvnUtil(QObject *parent)
    : QObject(parent),
      m_svnManager(Settings::get()->getSvnManager()),
      m_encoder(QTextCodec::codecForName("WINDOWS-1251")) {
  //Рабочая копия
  m_workingCopy = new QProcess(this);
  connect(m_workingCopy, &QProcess::readyReadStandardOutput, this,
          &SvnUtil::receivProcessOutput);
  connect(m_workingCopy, &QProcess::readyReadStandardError, this,
          &SvnUtil::receivProcessErrorOutput);
  connect(m_workingCopy, SIGNAL(stateChanged(QProcess::ProcessState)), this,
          SIGNAL(onStateChanged(QProcess::ProcessState)));
  connect(m_workingCopy, SIGNAL(error(QProcess::ProcessError)), this,
          SLOT(receivProcessError(QProcess::ProcessError)));
  connect(m_workingCopy, SIGNAL(finished(int, QProcess::ExitStatus)), this,
          SLOT(finishedProcess(int, QProcess::ExitStatus)));

  //Репозиторий
  m_repository = new QProcess(this);
  connect(m_repository, &QProcess::readyReadStandardOutput, this,
          &SvnUtil::receivProcessOutput);
  connect(m_repository, &QProcess::readyReadStandardError, this,
          &SvnUtil::receivProcessErrorOutput);
  connect(m_repository, SIGNAL(stateChanged(QProcess::ProcessState)), this,
          SIGNAL(onStateChanged(QProcess::ProcessState)));
  connect(m_repository, SIGNAL(error(QProcess::ProcessError)), this,
          SLOT(receivProcessError(QProcess::ProcessError)));
  connect(m_repository, SIGNAL(finished(int, QProcess::ExitStatus)), this,
          SLOT(finishedProcess(int, QProcess::ExitStatus)));
}

QStringList SvnUtil::arrayToStringList(const QByteArray &data) {
  QString str = m_encoder->toUnicode(data).trimmed();
  QStringList list = str.replace("\r\n", "\n").split('\n');
  QStringList tmpList;

  for (const QString &str : list) {
    if (!str.isEmpty()) {
      tmpList += getActionName(str);
    }
  }

  return tmpList;
}

QString SvnUtil::getActionName(const QString &str) {
  static const QString added = trUtf8("Добавлен");
  static const QString brokenLock = trUtf8("Broken lock");
  static const QString deleted = trUtf8("Удалён");
  static const QString updated = trUtf8("Обновлён");
  static const QString conflicted = trUtf8("Конфликт");
  static const QString merged = trUtf8("Объединён");
  static const QString existed = trUtf8("Существует");
  static const QString undefined = trUtf8("Undefined");

  const QRegExp rx("([ABDUCGE ]{1,4}) (.+)");

  if (rx.indexIn(str) >= 0) {  // rx.indexIn(str) >= 0
    QString status;
    QString tmpCap1 = rx.cap(1).remove(' ');
    if (tmpCap1.isEmpty()) return 0;

    char type = tmpCap1.at(0).toLatin1();
    switch (type) {
      case 'A':
        status = added;
        break;
      case 'B':
        status = brokenLock;
        break;
      case 'D':
        status = deleted;
        break;
      case 'U':
        status = updated;
        break;
      case 'C':
        status = conflicted;
        break;
      case 'G':
        status = merged;
        break;
      case 'E':
        status = existed;
        break;
      default:
        status = undefined;
    };

    QString tmpCap2 = rx.cap(2);
    if (tmpCap2.isEmpty()) return str;

    return status + " " + rx.cap(2);
  }

  return str;
}

qint32 SvnUtil::getRevisionFromXML(const QByteArray &data) {
  QXmlStreamReader xml(data);
  while (!xml.atEnd() && !xml.hasError()) {
    xml.readNext();
    if (xml.name() == "entry") {
      return xml.attributes().value("revision").toInt();
    }
  }
  return -1;
}

TypesAction SvnUtil::getActionFromError(const QByteArray &data) {
  QRegExp rx("(?:svn:.+[A-Z])([\\d]+)");

  if (rx.indexIn(data) >= 0) {
    uint idx = rx.cap(1).toUInt();

    switch (idx) {
      case 155036:
        return TypesAction::upgrade;
      case 155007:
        return TypesAction::checkout;
      case 155037:
      case 155004:
        return TypesAction::cleanup;
      case 175002:
        return TypesAction::null;
      default:
        return TypesAction::null;
    }
  }

  return TypesAction::null;
}

void SvnUtil::actionFromStatus(const QByteArray &data) {
  if (data.indexOf("tree-conflicted") >= 0) {
    LOG(WARNING) << trUtf8("Обнаружены конфликты в рабочей копии.");
    LOG(WARNING) << trUtf8("Требуется выполнить отмену всех изменений.");

    emit onRequiredAction(TypesAction::revert);
  } else if (data.indexOf("\"incomplete\"") >= 0) {
    LOG(WARNING) << trUtf8(
        "Невозможно получить корректную ревизию рабочей копии.");
    LOG(WARNING) << trUtf8(
        "Скорее всего, обновление/получение данных было прервано.");
    LOG(WARNING) << trUtf8("Запустите процесс обновления.");

    emit onRequiredAction(TypesAction::update);
  } else {
    executeAction(TypesAction::getInfoWorkingCopy);
  }
}

void SvnUtil::receivProcessOutput() {
  if (!sender()) return;

  QProcess *process = qobject_cast<QProcess *>(sender());
  const QString objName = process->objectName();

  if (objName == NamesObjects::INFO_WORKING_COPY)
    emit onWorkingCopyRev(getRevisionFromXML(process->readAllStandardOutput()));
  else if (objName == NamesObjects::STATUS_WORKING_COPY) {
    m_bufferWorkingCopyOutput += process->readAllStandardOutput();
  } else if (objName == NamesObjects::INFO_REPOSITORY) {
    emit onRepositoryRev(getRevisionFromXML(process->readAllStandardOutput()));
  } else {
    for (const QString &str :
         arrayToStringList(process->readAllStandardOutput())) {
      LOG(INFO) << str;
    }
  }
}

void SvnUtil::receivProcessErrorOutput() {
  if (!sender()) return;

  QProcess *process = qobject_cast<QProcess *>(sender());
  const QString &objName = process->objectName();

  if (objName == NamesObjects::INFO_REPOSITORY)
    m_bufferRepositoryError += process->readAllStandardError();
  else
    m_bufferWorkingCopyError += process->readAllStandardError();
}

void SvnUtil::receivProcessError(QProcess::ProcessError error) {
  if (!sender()) return;

  QProcess *process = qobject_cast<QProcess *>(sender());

  if (error == QProcess::Crashed) {
    LOG(ERROR) << trUtf8("Операция \"%1\" завершилась аварийно.")
                      .arg(process->objectName());
    return;
  }

  LOG(ERROR) << process->errorString();
  onRequiredAction(TypesAction::undefined);
}

void SvnUtil::finishedProcess(int exitCode, QProcess::ExitStatus exitStatus) {
  Q_UNUSED(exitCode)
  Q_UNUSED(exitStatus)

  if (!sender()) return;

  if (exitStatus == QProcess::CrashExit) return;

  const QString &objName = sender()->objectName();
  QByteArray bufferError;

  if (objName == NamesObjects::INFO_REPOSITORY)
    bufferError = m_bufferRepositoryError;
  else
    bufferError = m_bufferWorkingCopyError;

  if (!bufferError.isEmpty()) {
    for (const QString str : arrayToStringList(bufferError)) LOG(ERROR) << str;

    emit onRequiredAction(getActionFromError(bufferError));
  } else {
    if (objName == NamesObjects::INFO_WORKING_COPY) {
      emit onRequiredAction(TypesAction::success);
    } else if (objName == NamesObjects::STATUS_WORKING_COPY) {
      actionFromStatus(m_bufferWorkingCopyOutput);
    } else if (objName == NamesObjects::INFO_REPOSITORY) {
    } else {
      executeAction(TypesAction::getStatusWorkingCopy);
    }
  }
}

void SvnUtil::forcedStop() {
  LOG(WARNING) << trUtf8(
      "Принудительно останавливаем все выполнемые операции!");
  m_workingCopy->kill();
  m_repository->kill();
}

void SvnUtil::executeAction(TypesAction action) {
  if (action == TypesAction::getInfoRepository) {
    m_repository->close();
    m_repository->kill();
  } else {
    m_workingCopy->close();
    m_workingCopy->kill();
  }

  QString args;

  switch (action) {
    case TypesAction::getStatusWorkingCopy:
      LOG(INFO) << trUtf8("Запрашиваем статус рабочей копии.");
      m_bufferWorkingCopyError.clear();
      m_bufferWorkingCopyOutput.clear();
      args = QString("%1 \"%2\"")
                 .arg(SvnCommands::STATUS)
                 .arg(Settings::get()->getPathPackages());

      m_workingCopy->setObjectName(NamesObjects::STATUS_WORKING_COPY);
      break;

    case TypesAction::getInfoWorkingCopy:
      LOG(INFO) << trUtf8("Запрашиваем информацию о рабочей копии.");
      m_bufferWorkingCopyError.clear();
      args = QString("%1 \"%2\"")
                 .arg(SvnCommands::INFO)
                 .arg(Settings::get()->getPathPackages());

      m_workingCopy->setObjectName(NamesObjects::INFO_WORKING_COPY);
      break;
    case TypesAction::getInfoRepository:
      LOG(INFO) << trUtf8("Запрашиваем информацию о репозитории.");
      m_bufferRepositoryError.clear();
      args = QString("%1 \"%2\"")
                 .arg(SvnCommands::INFO)
                 .arg(m_svnManager.getUrl());

      m_repository->setObjectName(NamesObjects::INFO_REPOSITORY);
      m_repository->setNativeArguments(args);
      m_repository->start(SvnParams::SVN_UTIL);
      return;
    case TypesAction::update:
      LOG(INFO) << trUtf8(
          "Запускаем процесс обновления рабочей копии (update).");
      m_bufferWorkingCopyError.clear();
      args = QString("%1 \"%2\"")
                 .arg(SvnCommands::UPDATE)
                 .arg(Settings::get()->getPathPackages());

      m_workingCopy->setObjectName(NamesObjects::UPDATE);
      break;
    case TypesAction::upgrade:
      LOG(INFO) << trUtf8("Обновляем формат рабочей копии (upgrade).");
      m_bufferWorkingCopyError.clear();
      args = QString("%1 \"%2\"")
                 .arg(SvnCommands::UPGRADE)
                 .arg(Settings::get()->getPathPackages());

      m_workingCopy->setObjectName(NamesObjects::UPGRADE);
      break;
    case TypesAction::checkout:
      LOG(INFO) << trUtf8("Извлекаем рабочую копию (checkout).");
      m_bufferWorkingCopyError.clear();
      args = QString("%1 %2 \"%3\"")
                 .arg(SvnCommands::CHECKOUT)
                 .arg(m_svnManager.getUrl())
                 .arg(Settings::get()->getPathPackages());

      m_workingCopy->setObjectName(NamesObjects::CHECKOUT);
      break;
    case TypesAction::cleanup:
      LOG(INFO) << trUtf8("Убираем блокировки рабочей копии (cleanup).");
      m_bufferWorkingCopyError.clear();
      args = QString("%1 \"%2\"")
                 .arg(SvnCommands::CLEANUP)
                 .arg(Settings::get()->getPathPackages());

      m_workingCopy->setObjectName(NamesObjects::CLEANUP);
      break;
    case TypesAction::revert:
      LOG(INFO) << trUtf8("Отменяем изменения рабочей копии (revert).");
      m_bufferWorkingCopyError.clear();
      args = QString("%1 \"%2\"")
                 .arg(SvnCommands::REVERT)
                 .arg(Settings::get()->getPathPackages());

      m_workingCopy->setObjectName(NamesObjects::REVERT);
      break;
    case TypesAction::relocate:
      LOG(INFO) << trUtf8("Перебазируем рабочую копию (relocate).");
      m_bufferWorkingCopyError.clear();
      args = QString("%1 %2 \"%3\"")
                 .arg(SvnCommands::RELOCATE)
                 .arg(m_svnManager.getUrl())
                 .arg(Settings::get()->getPathPackages());

      m_workingCopy->setObjectName(NamesObjects::RELOCATE);
      break;
    default:
      break;
  }

  m_workingCopy->setNativeArguments(args);
  m_workingCopy->start(SvnParams::SVN_UTIL);
}

void SvnUtil::getInfoWorkingCopy() {
  executeAction(TypesAction::getStatusWorkingCopy);
}

void SvnUtil::getInfoRepository() {
  executeAction(TypesAction::getInfoRepository);
}

void SvnUtil::update() { executeAction(TypesAction::update); }

void SvnUtil::upgrade() { executeAction(TypesAction::upgrade); }

void SvnUtil::checkout() { executeAction(TypesAction::checkout); }

void SvnUtil::cleanup() { executeAction(TypesAction::cleanup); }

void SvnUtil::revert() { executeAction(TypesAction::revert); }

void SvnUtil::relocate() { executeAction(TypesAction::relocate); }

void SvnUtil::deleteSvnDB() {
  LOG(INFO) << trUtf8("Удаляем базу данных «.svn» рабочей копии.");
  QApplication::processEvents(QEventLoop::AllEvents);
  QDir(Settings::get()->getPathPackages() + "/.svn").removeRecursively();
  executeAction(TypesAction::getStatusWorkingCopy);
}

QProcess::ProcessState SvnUtil::stateRunningProcess() {
  bool workingCopy = m_workingCopy->state() == QProcess::Running;
  bool repository = m_repository->state() == QProcess::Running;

  if (workingCopy || repository) {
    return QProcess::Running;
  } else {
    return QProcess::NotRunning;
  }
}
