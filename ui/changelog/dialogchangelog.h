#pragma once

// Project

// STL

// Native

// Qt
#include <QDialog>

namespace Ui {
class DialogChangelog;
}

class DialogChangelog : public QDialog {
  Q_OBJECT

 public:
  explicit DialogChangelog(QWidget *parent = 0);
  ~DialogChangelog();

 private:
  Ui::DialogChangelog *ui;
};
