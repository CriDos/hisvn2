// Project
#include "dialogmakeandbuild.h"
#include "settings/settings.h"
#include "statuswidget.h"
#include "ui_dialogmakeandbuild.h"

// LOG
#include "logger.h"

// STL
#include <utility>

// Native

// Qt
#include <QCloseEvent>
#include <QDebug>
#include <QProcess>
#include <QScrollBar>
#include <QTextEdit>
#include <QToolButton>
#include <QtCore>

DialogMakeAndBuild::DialogMakeAndBuild(QWidget *parent)
    : QDialog(parent),
      m_encoderCmd(QTextCodec::codecForName("IBM 866")),
      ui(new Ui::DialogMakeAndBuild) {
  ui->setupUi(this);
  setWindowFlags(windowFlags() & ~Qt::WindowContextHelpButtonHint);

  ui->tableWidget->horizontalHeader()->setSectionResizeMode(
      0, QHeaderView::Stretch);
  ui->tableWidget->horizontalHeader()->setSectionResizeMode(
      1, QHeaderView::ResizeToContents);

  m_process = new QProcess(this);
  connect(m_process, SIGNAL(finished(int, QProcess::ExitStatus)), this,
          SLOT(processFinished(int, QProcess::ExitStatus)));
  connect(m_process, &QProcess::readyReadStandardOutput, this,
          &DialogMakeAndBuild::receivProcessOutput);
  connect(m_process, &QProcess::readyReadStandardError, this,
          &DialogMakeAndBuild::receivProcessErrorOutput);

  initPackagesList();
  addTableData();
  addButtonsPack();
  setMinimumSize(sizeHint());
}

DialogMakeAndBuild::~DialogMakeAndBuild() { delete ui; }

void DialogMakeAndBuild::disableActionUi(bool state) {
  ui->pushButton_run->setDisabled(state);

  m_disableExit = state;
}

bool DialogMakeAndBuild::isDisableExit() const { return m_disableExit; }

void DialogMakeAndBuild::receivProcessOutput() {
  QString logMessage =
      m_encoderCmd->toUnicode(m_process->readAllStandardOutput());

  m_currentStatusWidget->addOutputText(logMessage);
}

void DialogMakeAndBuild::receivProcessErrorOutput() {
  QString logMessage =
      m_encoderCmd->toUnicode(m_process->readAllStandardError());

  m_currentStatusWidget->addErrorText(logMessage);
}

void DialogMakeAndBuild::processFinished(int resultCode,
                                         QProcess::ExitStatus status) {
  Q_UNUSED(resultCode)

  m_currentStatusWidget->setCommandText(m_command);
  if (status == QProcess::QProcess::CrashExit ||
      !m_currentStatusWidget->emtyError())
    m_currentStatusWidget->setWarning();
  else
    m_currentStatusWidget->setOk();

  nextExecution();
}

void DialogMakeAndBuild::onPackClicked() {
  auto *button = qobject_cast<QPushButton *>(sender());
  if (button) {
    ui->tableWidget->checkedOnlyPack(button->text());
  }
}

QList<QPair<QString, QString>> DialogMakeAndBuild::getBatFiles() {
  const QStringList excludeDirs = Settings::get()->getExcludeDirs();
  const QStringList findBatFiles = Settings::get()->getFindBatFiles();
  QList<QPair<QString, QString>> resultListFiles;

  for (const QString &pack : m_packagesList) {
    QDir packageDir(Settings::get()->getPathPackages() + QDir::separator() +
                    pack);
    packageDir.setFilter(QDir::AllDirs | QDir::NoDotAndDotDot);

    QDirIterator it(packageDir.path(), findBatFiles, QDir::Files);
    while (it.hasNext()) {
      QFileInfo fInfo = it.next();
      resultListFiles.append(
          {pack, QDir::toNativeSeparators(fInfo.filePath())});
    }

    QStringList listPathDirs;
    for (const QFileInfo &dir : packageDir.entryInfoList()) {
      if (!excludeDirs.contains(dir.fileName(), Qt::CaseInsensitive)) {
        listPathDirs << dir.filePath();
      }
    }

    for (const QString &pathDir : listPathDirs) {
      QDirIterator it(pathDir, findBatFiles, QDir::Files,
                      QDirIterator::Subdirectories);

      while (it.hasNext()) {
        QFileInfo fInfo = it.next();
        resultListFiles.append(
            {pack, QDir::toNativeSeparators(fInfo.filePath())});
      }
    }
  }

  return resultListFiles;
}

void DialogMakeAndBuild::initPackagesList() {
  m_packagesList.clear();
  const QStringList excludePackDirs = Settings::get()->getExcludePackDirs();

  QDir packagesDir(Settings::get()->getPathPackages());
  packagesDir.setFilter(QDir::AllDirs | QDir::NoDotAndDotDot);

  for (QFileInfo pathInfo : packagesDir.entryInfoList()) {
    if (!excludePackDirs.contains(pathInfo.fileName(), Qt::CaseInsensitive)) {
      m_packagesList << pathInfo.baseName();
    }
  }
}

void DialogMakeAndBuild::addTableData() {
  auto *table = ui->tableWidget;
  for (const auto &pair : getBatFiles()) {
    table->addItem(pair.first, pair.second);
  }
}

void DialogMakeAndBuild::addButtonsPack() {
  for (const QString &pack : m_packagesList) {
    auto *button = new QPushButton;
    button->setAutoDefault(false);
    button->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);
    button->setToolTip(trUtf8("Выбрать только пакет <b>%1</b>").arg(pack));
    connect(button, &QPushButton::clicked, this,
            &DialogMakeAndBuild::onPackClicked);
    button->setText(pack);
    ui->verticalLayout_packs->addWidget(button);
  }
}

void DialogMakeAndBuild::nextExecution() {
  auto *table = ui->tableWidget;

  if (table->getNextRowIdx() == -1) {
    table->resetAllStatus();
  }

  if (table->hasNext()) {
    int nextRow = table->next();
    if (table->getCheckStateItem(nextRow) != Qt::Checked) {
      nextExecution();
      return;
    }
    table->selectRow(nextRow);

    m_currentStatusWidget = table->currentStatusWidget();
    m_currentStatusWidget->setExecuting();

    QFileInfo file(table->currentFilePath());
    QString nativePath = QDir::toNativeSeparators(file.absolutePath());
    m_command = QString("cmd /c \"cd /d %1 && @echo | call %2\"")
                    .arg(nativePath)
                    .arg(file.fileName());
    m_process->start(m_command);
  } else {
    table->resetSelection();
    disableActionUi(false);
  }
}

void DialogMakeAndBuild::on_pushButton_run_clicked() {
  disableActionUi(true);
  nextExecution();
}

void DialogMakeAndBuild::closeEvent(QCloseEvent *event) {
  if (isDisableExit()) {
    event->ignore();
  }
}

void DialogMakeAndBuild::on_pushButton_allChecked_clicked() {
  ui->tableWidget->setAllCheckState(Qt::Checked);
}

void DialogMakeAndBuild::on_pushButton_allUncheck_clicked() {
  ui->tableWidget->setAllCheckState(Qt::Unchecked);
}
