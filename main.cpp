// Project
#include "qtsingleapplication.h"
#include "ui/mainwindow/mainwindow.h"

// LOG
#include "logger.h"

// STL

// Native

// Qt
#include <QApplication>
#include <QDir>
#include <QLocale>
#include <QTranslator>

int main(int argc, char *argv[]) {
  //Инициализация логгера
  LogHandler::initialize(argc, argv);

  //Запуск QtSingleApplication
  QtSingleApplication a(argc, argv);
  a.setApplicationName(APP_PRODUCT);
  a.setApplicationVersion(APP_VERSION);

  if (a.isRunning()) {
    a.sendMessage("");
    return 0;
  }

  //Загрузка переводов
  QTranslator translator;
  translator.load(":/tr/translations/qtbase_ru.qm");
  a.installTranslator(&translator);

  //Задаём информацию о приложении
  QCoreApplication::setOrganizationName(APP_COMPANY);
  QCoreApplication::setOrganizationDomain(APP_COPYRIGHT);
  QCoreApplication::setApplicationName(APP_PRODUCT);

  //Запускаем главное окно приложения
  MainWindow w;
  a.setActivationWindow(&w);
  w.show();

  return a.exec();
}
