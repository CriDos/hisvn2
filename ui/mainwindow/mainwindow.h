#pragma once

// Project
#include "util/svnutil.h"

class NetworkTest;
class SvnManager;
class Autoupdate;

// STL

// Native

// Qt
#include <QListWidgetItem>
#include <QMainWindow>
#include <QStack>

namespace Ui {
class MainWindow;
}

enum class ButtonActionState {
  undefined,
  update,
  checkout,
  upgrade,
  clenup,
  revert,
};

class MainWindow : public QMainWindow {
  Q_OBJECT

 private:
  Ui::MainWindow *ui{};
  NetworkTest *m_networkTest{};
  SvnUtil *m_svnUtil{};
  QMenu *m_menuTools{};
  Autoupdate *m_autoupdate{};
  ButtonActionState m_buttonAction{};

 public:
  explicit MainWindow(QWidget *parent = 0);
  ~MainWindow();

 private:
  void saveSettings();
  void loadSettiongs();
  void appFontSizeUpdate(int size);
  void setDisableActionUi(bool state);
  void setActionButton(ButtonActionState state);
  void resetResultLabels();
  void runNetworkTest();
  void updateSvnInfo();

 private slots:
  void receivStateNetwork(bool state);
  void receivStateSvnHost(bool state);
  void receivRequiredAction(TypesAction type);
  void receivWorkingCopyRev(qint32 rev);
  void receivRepositoryRev(qint32 rev);
  void receivStateChanged(QProcess::ProcessState state);
  void updateInfo();
  void svnSelected();

 protected:
  void closeEvent(QCloseEvent *event);

 private slots:
  void on_pushButton_updateInfo_clicked();
  void on_pushButton_clearLog_clicked();
  void on_pushButton_actions_clicked();
  void on_pushButton_copyLog_clicked();
  void on_pushButton_forcedStop_clicked();
  void on_action_relocate_triggered();
  void on_action_revert_triggered();
  void on_action_deleteSvnDB_triggered();
  void on_action_makeAndBuild_triggered();
  void on_action_qt_triggered();
  void on_action_about_triggered();
  void on_action_svnManager_triggered();
  void on_action_update_triggered();
  void on_action_changelog_triggered();
  void on_action_bugreport_triggered();
  void on_action_settings_triggered();
};
