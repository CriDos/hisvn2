#pragma once

// Project
#include "settings_keys.h"
#include "svnmanager.h"

// LOG

// STL

// Native

// Qt
#include <QObject>
#include <QSettings>
#include <QWidget>

class Settings : public QObject {
  Q_OBJECT

 private:
  Q_DISABLE_COPY(Settings)

 private:
  QSettings m_settings;
  SvnManager m_svnManager;
  const QString m_defaultPackagesDir = "Elements";
  const QStringList m_defaultExcludeDirs{"Example", "examples", "conf",
                                         "code",    "icon",     ".svn"};
  const QStringList m_defaultFindBatFiles{"sql_to_db.bat", "make.bat"};
  const QStringList m_defaultExcludePackDirs{"_base", "RTCG", "RTCG_Win_fork",
                                             "FTCG", "Modules"};

 signals:
  void onChangeStateAutoupdate(bool state);
  void onAppFontSizeUpdate(int size);
  void onChangePathPackages(QString path);
  void onChangeExcludeDirs(QStringList list);
  void onChangeFindBatFiles(QStringList list);
  void onChangeExcludePackDirs(QStringList list);

 public:
  explicit Settings();

 public slots:
  static Settings *get();
  SvnManager &getSvnManager();

  void setStateAutoupdate(bool state);
  bool getStateAutoupdate();

  void saveGeometryWidget(const QWidget &widget);
  void loadGeometryWidget(QWidget &widget);

  int getAppFontSize();
  void saveAppFontSize();
  void saveAppFontSize(int size);
  void loadAppFontSize();

  QString getPathPackages() const;
  void setPathPackages(const QString &pathPackages);
  void resetPathPackages();

  QStringList getExcludeDirs();
  void setExcludeDirs(QStringList list);
  void resetExcludeDirs();

  QStringList getFindBatFiles();
  void setFindBatFiles(QStringList list);
  void resetFindBatFiles();

  QStringList getExcludePackDirs();
  void setExcludePackDirs(QStringList list);
  void resetExcludePackDirs();
};
