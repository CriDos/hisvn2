// Project
#include "logger.h"

// LOG
INITIALIZE_EASYLOGGINGPP

// STL

// Native

// Qt
#include <QDateTime>
#include <QScrollBar>
#include <QTextEdit>

static QTextEdit *m_logWidget{};

void LogHandler::handle(const el::LogDispatchData *data) {
  if (m_logWidget == nullptr) return;

  //Формирование сообщения
  const el::LogMessage *logMessage = data->logMessage();
  QChar level;
  switch (logMessage->level()) {
    case el::Level::Info:
      level = QLatin1Char('I');
      break;
    case el::Level::Warning:
      level = QLatin1Char('W');
      break;
    case el::Level::Error:
      level = QLatin1Char('E');
      break;
    case el::Level::Debug:
      level = QLatin1Char('D');
      break;
    case el::Level::Fatal:
      level = QLatin1Char('F');
      break;
    case el::Level::Global:
      level = QLatin1Char('G');
      break;
    case el::Level::Trace:
      level = QLatin1Char('T');
      break;
    case el::Level::Verbose:
      level = QLatin1Char('V');
      break;
    case el::Level::Unknown:
      level = QLatin1Char('U');
      break;
  }

  QString time =
      QDateTime::currentDateTime().toString("dd.MM.yyyy_hh:mm:ss.zzz");
  QString message = QString("%1 %2: %3\n")
                        .arg(time)
                        .arg(level)
                        .arg(QString::fromStdString(logMessage->message()));

  QColor textColor;
  switch (logMessage->level()) {
    case el::Level::Info:
      textColor = "black";
      break;
    case el::Level::Warning:
      textColor = "#D86500";
      break;
    default:
      textColor = "red";
      break;
  }

  //Параметры лога
  QScrollBar *pScrollBar = m_logWidget->verticalScrollBar();
  bool boolAtBottom = (pScrollBar->value() == pScrollBar->maximum());

  QTextCursor textCursor = QTextCursor(m_logWidget->document());
  textCursor.movePosition(QTextCursor::End);

  m_logWidget->setTextCursor(textCursor);
  m_logWidget->setTextColor(textColor);
  m_logWidget->insertPlainText(message);

  if (boolAtBottom) pScrollBar->setValue(pScrollBar->maximum());
}

void LogHandler::initialize(int argc, char *argv[]) {
  QDir makeLogDir;
  makeLogDir.mkdir("logs");

  START_EASYLOGGINGPP(argc, argv);

  el::Configurations conf;
  conf.setGlobally(el::ConfigurationType::Filename, "logs/%datetime.log");
  conf.setGlobally(el::ConfigurationType::Format,
                   "%datetime{%d.%M.%y_%h:%m:%s.%z};%loc;%levshort: %msg");

  el::Logger *defaultLogger = el::Loggers::getLogger("default");
  defaultLogger->configure(conf);

  el::Loggers::removeFlag(el::LoggingFlag::NewLineForContainer);
  el::Loggers::addFlag(el::LoggingFlag::ColoredTerminalOutput);
  el::Loggers::addFlag(el::LoggingFlag::LogDetailedCrashReason);

  el::Helpers::installLogDispatchCallback<LogHandler>("LogHandler");
  LogHandler *logHandler =
      el::Helpers::logDispatchCallback<LogHandler>("LogHandler");
  logHandler->setEnabled(true);

  LOG(INFO) << "Логгер запущен!";
}

void LogHandler::setLogWidget(QTextEdit *logWidget) { m_logWidget = logWidget; }
