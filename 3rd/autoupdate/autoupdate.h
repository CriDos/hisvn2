#pragma once

// Project

// LOG
#include "logger.h"

// STL

// Native

// Qt
#include <QObject>
#include <QProgressDialog>
#include <QtNetwork>

class Autoupdate : public QObject {
  Q_OBJECT
  Q_DISABLE_COPY(Autoupdate)

 private:
  QNetworkAccessManager *m_manager = nullptr;
  QNetworkReply *m_reply = nullptr;
  QProgressDialog *m_downloadProgressDialog = nullptr;
  QUrl m_urlVersion;
  QUrl m_urlProgram;
  const int m_timeout = 5000;
  QTimer m_timerTimeout;
  QString m_newVersion;
  int m_newFileSize;
  QString m_newFileHash;
  bool m_silent = false;

 public:
  explicit Autoupdate(QObject *parent = 0);

 private:
  void updateInterrupted(const QString &str, const QString str2 = QString());
  void initDownloadProgressDialog();
  void timeout();

  void readFileFinished();
  void processingUpdate(QByteArray data);
  void downloadProgress(qint64 bytesRead, qint64 totalBytes);
  void goUpdate();

  void processingVersion();
  bool parseVersion(QByteArray data);
  void readVersionFinished();

 public:
  void checkUpdate(bool silent = false);
};
