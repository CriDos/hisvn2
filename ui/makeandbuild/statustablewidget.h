#pragma once
// Project
#include "statuswidget.h"

// STL

// Native

// Qt
#include <QTableWidget>

class StatusTableWidget : public QTableWidget {
  Q_OBJECT

 private:
  int m_nextRowIdx = -1;

 public:
  explicit StatusTableWidget(QWidget *parent = 0);

 public:
  void checkedOnlyPack(const QString &pack);
  Qt::CheckState getCheckStateItem(int idx);
  void setAllCheckState(Qt::CheckState state);
  void resetSelection();
  void resetAllStatus();
  void addItem(const QString &pack, const QString &path);
  StatusWidget *currentStatusWidget();
  QString currentFilePath();
  bool hasNext();
  int next();
  int getNextRowIdx() const;
  void setNextRowIdx(int nextRowIdx);

  // QWidget interface
 protected:
  void mousePressEvent(QMouseEvent *event) override;
  void mouseMoveEvent(QMouseEvent *event) override;
};
