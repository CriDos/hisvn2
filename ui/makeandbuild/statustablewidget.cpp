// Project
#include "statustablewidget.h"
#include "statuswidget.h"

// STL

// Native

// Qt
#include <QHeaderView>
#include <QMouseEvent>

StatusTableWidget::StatusTableWidget(QWidget *parent) : QTableWidget(parent) {
  auto *verticalHeader = this->verticalHeader();
  verticalHeader->setSectionsClickable(false);
  verticalHeader->setSectionResizeMode(QHeaderView::Fixed);

  auto *horizontalHeader = this->horizontalHeader();
  horizontalHeader->setSectionsClickable(false);
}

void StatusTableWidget::checkedOnlyPack(const QString &pack) {
  setAllCheckState(Qt::Unchecked);
  int countRow = rowCount() - 1;
  for (int i = 0; i <= countRow; ++i) {
    auto *item = this->item(i, 0);
    if (item->data(100).toString() == pack) {
      item->setCheckState(Qt::Checked);
      scrollToItem(item);
    }
  }
}

Qt::CheckState StatusTableWidget::getCheckStateItem(int idx) {
  auto *item = this->item(idx, 0);
  if (item) return item->checkState();

  return Qt::PartiallyChecked;
}

void StatusTableWidget::setAllCheckState(Qt::CheckState state) {
  int countRow = rowCount() - 1;
  for (int i = 0; i <= countRow; ++i) {
    auto *item = this->item(i, 0);
    item->setCheckState(state);
  }
}

void StatusTableWidget::resetSelection() {
  clearSelection();
  setCurrentCell(-1, -1);
  setNextRowIdx(-1);
}

void StatusTableWidget::resetAllStatus() {
  resetSelection();

  int countRow = rowCount() - 1;
  for (int i = 0; i <= countRow; ++i) {
    StatusWidget *statusWidget = qobject_cast<StatusWidget *>(cellWidget(i, 1));
    statusWidget->reset();
  }
}

void StatusTableWidget::addItem(const QString &pack, const QString &path) {
  int countRow = rowCount();
  int newCountRow = countRow + 1;
  int newRowId = newCountRow - 1;

  auto *item = new QTableWidgetItem(path);
  item->setCheckState(Qt::Checked);
  item->setData(100, pack);

  setRowCount(newCountRow);
  setItem(newRowId, 0, item);
  setCellWidget(newRowId, 1, new StatusWidget);
}

StatusWidget *StatusTableWidget::currentStatusWidget() {
  auto *currentWidget = cellWidget(currentRow(), 1);

  if (!currentWidget) return nullptr;

  return qobject_cast<StatusWidget *>(currentWidget);
}

QString StatusTableWidget::currentFilePath() {
  auto *tableItem = item(currentRow(), 0);

  if (!tableItem) return QString();

  return tableItem->text();
}

bool StatusTableWidget::hasNext() { return (m_nextRowIdx <= rowCount()); }

int StatusTableWidget::next() { return m_nextRowIdx++; }

int StatusTableWidget::getNextRowIdx() const { return m_nextRowIdx; }

void StatusTableWidget::setNextRowIdx(int nextRowIdx) {
  m_nextRowIdx = nextRowIdx;
}

void StatusTableWidget::mousePressEvent(QMouseEvent *event) { Q_UNUSED(event) }

void StatusTableWidget::mouseMoveEvent(QMouseEvent *event) { Q_UNUSED(event) }
