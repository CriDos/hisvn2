// Project
#include "networktest.h"

// LOG
#include "logger.h"

// STL

// Native

// Qt
#include <QHostAddress>

NetworkTest::NetworkTest(QObject *parent)
    : QObject(parent), m_svnManager(Settings::get()->getSvnManager()) {
  //Содание сокетов
  m_networkSocket = new QTcpSocket(this);
  m_svnHostSocket = new QTcpSocket(this);

  //Подключения

  //!Таймер
  connect(&m_networkTimeOut, SIGNAL(timeout()), this, SLOT(networkTimeout()));
  connect(&m_svnHostTimeOut, SIGNAL(timeout()), this, SLOT(svnHostTimeout()));

  //!Сокеты
  connect(m_networkSocket, &QTcpSocket::connected, this,
          &NetworkTest::networkOk);
  connect(m_networkSocket, SIGNAL(error(QAbstractSocket::SocketError)), this,
          SLOT(networkError()));

  connect(m_svnHostSocket, &QTcpSocket::connected, this,
          &NetworkTest::svnHostOk);
  connect(m_svnHostSocket, SIGNAL(error(QAbstractSocket::SocketError)), this,
          SLOT(svnHostError()));
}

NetworkTest::~NetworkTest() {}

void NetworkTest::networkOk() {
  m_networkTimeOut.stop();
  m_networkSocket->abort();

  emit onStateNetwork(true);
}

void NetworkTest::networkError() {
  m_networkTimeOut.stop();
  m_networkSocket->abort();
  LOG(ERROR) << QString("Test network: %1").arg(m_networkSocket->errorString());

  emit onStateNetwork(false);
}

void NetworkTest::networkTimeout() {
  m_networkTimeOut.stop();
  m_networkSocket->abort();
  LOG(ERROR) << QString("Test network: Connection timed out: %1")
                    .arg(NetParams::TIMEOUT);

  emit onStateNetwork(false);
}

void NetworkTest::svnHostOk() {
  m_svnHostTimeOut.stop();
  m_svnHostSocket->abort();

  emit onStateSvnHost(true);
}

void NetworkTest::svnHostError() {
  m_svnHostTimeOut.stop();
  m_svnHostSocket->abort();

  LOG(ERROR) << QString("Test SVN host: \"%1\"").arg(m_svnManager.getHost());
  LOG(ERROR) << QString("Test SVN: %1)").arg(m_svnHostSocket->errorString());

  emit onStateSvnHost(false);
}

void NetworkTest::svnHostTimeout() {
  m_svnHostTimeOut.stop();
  m_svnHostSocket->abort();

  LOG(ERROR) << QString("Test SVN host: \"%1\"").arg(m_svnManager.getHost());
  LOG(ERROR) << QString("Test SVN: Connection timed out: %1 ms.")
                    .arg(NetParams::TIMEOUT);

  emit onStateSvnHost(false);
}

void NetworkTest::svnHostInfo(QHostInfo host) {
  if (host.error() != QHostInfo::NoError) {
    LOG(ERROR) << QString("Test SVN host: \"%1\"").arg(m_svnManager.getHost());
    LOG(ERROR)
        << QString("Test SVN: Lookup failed: %1").arg(host.errorString());

    emit onStateSvnHost(false);
  } else {
    if (!host.addresses().empty()) {
      m_svnHostSocket->abort();
      m_svnHostTimeOut.start(NetParams::TIMEOUT);
      m_svnHostSocket->connectToHost(host.addresses().at(0),
                                     NetParams::HTTP_PORT, QIODevice::ReadOnly);
    }
  }
}

void NetworkTest::testNetwork() {
  m_networkSocket->abort();
  m_networkTimeOut.start(NetParams::TIMEOUT);
  m_networkSocket->connectToHost(QHostAddress(NetParams::NETWORK_IP),
                                 NetParams::SVN_PORT, QIODevice::ReadOnly);
}

void NetworkTest::testSvnHost() {
  QHostInfo::lookupHost(m_svnManager.getHost(), this,
                        SLOT(svnHostInfo(QHostInfo)));
}
