#include "dialogviewlog.h"
#include "ui_dialogviewlog.h"

DialogViewLog::DialogViewLog(const QString &log, const QString &commandText,
                             QWidget *parent)
    : QDialog(parent), ui(new Ui::DialogViewLog) {
  ui->setupUi(this);
  setWindowFlags(windowFlags() & ~Qt::WindowContextHelpButtonHint |
                 Qt::WindowMinMaxButtonsHint);

  ui->lineEdit_command->setText(commandText);
  ui->textEdit_viewLog->setHtml(log);
}

DialogViewLog::~DialogViewLog() { delete ui; }
