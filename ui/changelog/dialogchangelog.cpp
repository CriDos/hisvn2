// Project

// STL

// Native

// Qt
#include "dialogchangelog.h"
#include "ui_dialogchangelog.h"

DialogChangelog::DialogChangelog(QWidget *parent)
    : QDialog(parent), ui(new Ui::DialogChangelog) {
  ui->setupUi(this);
  setWindowFlags(windowFlags() & ~Qt::WindowContextHelpButtonHint);

  //Обновление размера шрифта
  int sizeFont = QApplication::font().pointSize();
  QTextCursor cursor = ui->textEdit_changelog->textCursor();
  ui->textEdit_changelog->selectAll();
  ui->textEdit_changelog->setFontPointSize(sizeFont);
  ui->textEdit_changelog->setTextCursor(cursor);
}

DialogChangelog::~DialogChangelog() { delete ui; }
