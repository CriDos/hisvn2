#pragma once

// Project
#define ELPP_NO_DEFAULT_LOG_FILE
#include "easylogging++.h"

// LOG

// STL

// Native

// Qt
#include <QtCore>
class QTextEdit;

class LogHandler : public el::LogDispatchCallback {
 public:
  void handle(const el::LogDispatchData *data);

 public:
  static void initialize(int argc, char *argv[]);
  static void setLogWidget(QTextEdit *logWidget);
};
