#pragma once
// Project
#include "settings/svnmanager.h"
#include "svnutil.h"

// STL

// Native

// Qt
#include <QHostInfo>
#include <QTcpSocket>
#include <QTimer>

namespace NetParams {
Q_CONSTEXPR int HTTP_PORT = 80;
Q_CONSTEXPR int SVN_PORT = 53;
Q_CONSTEXPR int TIMEOUT = 5000;             //Таймаут
Q_CONSTEXPR char NETWORK_IP[] = "8.8.8.8";  // Google DNS сервер
}

class NetworkTest : public QObject {
  Q_OBJECT

 private:
  const SvnManager &m_svnManager;
  QTimer m_networkTimeOut;
  QTimer m_svnHostTimeOut;
  QTcpSocket *m_networkSocket;
  QTcpSocket *m_svnHostSocket;

 public:
  explicit NetworkTest(QObject *parent = 0);
  ~NetworkTest();

 signals:
  void onStateNetwork(bool);
  void onStateSvnHost(bool);

 private slots:
  void networkOk();
  void networkError();
  void networkTimeout();

  void svnHostOk();
  void svnHostError();
  void svnHostTimeout();
  void svnHostInfo(QHostInfo host);

 public slots:
  void testNetwork();
  void testSvnHost();
};
