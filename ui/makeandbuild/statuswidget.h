#pragma once

// Project
class StatusTableWidget;

// STL

// Native

// Qt
#include <QPixmap>
#include <QWidget>

namespace Ui {
class StatusWidget;
}

class StatusWidget : public QWidget {
  Q_OBJECT

 private:
  Ui::StatusWidget *ui;
  QPixmap m_waitPixmap;
  QPixmap m_executingPixmap;
  QPixmap m_warningPixmap;
  QPixmap m_okPixmap;
  bool m_noError = true;
  QString m_resultText;
  QString m_commandText;

 public:
  explicit StatusWidget();
  ~StatusWidget();

 public slots:
  void setWait();
  void setExecuting();
  void setWarning();
  void setOk();
  void addErrorText(QString str);
  void addOutputText(QString str);
  void setCommandText(const QString &str);
  bool emtyError();
  void reset();

 private slots:
  void on_toolButton_log_clicked();
};
