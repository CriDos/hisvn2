// Project
#include "mainwindow.h"
#include "autoupdate.h"
#include "settings/settings.h"
#include "ui/changelog/dialogchangelog.h"
#include "ui/makeandbuild/dialogmakeandbuild.h"
#include "ui/settings/dialogsettings.h"
#include "ui/svnmanager/dialogsvnmanager.h"
#include "ui_mainwindow.h"
#include "util/networktest.h"

// LOG
#include "logger.h"

// STL

// Native

// Qt
#include <QClipboard>
#include <QCloseEvent>
#include <QDesktopServices>
#include <QMenu>
#include <QMessageBox>

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent), ui(new Ui::MainWindow) {
  ui->setupUi(this);

  // Инициализация лога
  LogHandler::setLogWidget(ui->textEdit_log);

  //! --- Инициализация приложения ---
  LOG(INFO) << trUtf8("Инициализация приложения...");

  setWindowTitle(QString("%1 %2 - %3")
                     .arg(APP_PRODUCT)
                     .arg(APP_VERSION)
                     .arg(trUtf8("обновление элементов HiAsm")));
  setWindowFlags(Qt::WindowMinimizeButtonHint | Qt::WindowCloseButtonHint);

  LOG(INFO) << trUtf8("Версия приложения: %1").arg(APP_VERSION);

  //Создание обновлятора
  m_autoupdate = new Autoupdate(this);
  //Проверка обновления
  if (Settings::get()->getStateAutoupdate()) m_autoupdate->checkUpdate(true);

  //Создаём тесты сети и SVN сервера
  m_networkTest = new NetworkTest(this);
  //Подключаем тест сети и SVN сервера
  connect(m_networkTest, &NetworkTest::onStateNetwork, this,
          &MainWindow::receivStateNetwork);
  connect(m_networkTest, &NetworkTest::onStateSvnHost, this,
          &MainWindow::receivStateSvnHost);

  //Создаём и подключаем класс для работы с SVN
  m_svnUtil = new SvnUtil;
  connect(m_svnUtil, &SvnUtil::onRequiredAction, this,
          &MainWindow::receivRequiredAction);
  connect(m_svnUtil, &SvnUtil::onStateChanged, this,
          &MainWindow::receivStateChanged);
  connect(m_svnUtil, &SvnUtil::onWorkingCopyRev, this,
          &MainWindow::receivWorkingCopyRev);
  connect(m_svnUtil, &SvnUtil::onRepositoryRev, this,
          &MainWindow::receivRepositoryRev);

  //Обновление информации
  updateInfo();

  //Загрузка параметров Ui
  connect(Settings::get(), &Settings::onAppFontSizeUpdate, this,
          &MainWindow::appFontSizeUpdate);
  loadSettiongs();
}

MainWindow::~MainWindow() {
  LogHandler::setLogWidget(nullptr);
  delete ui;
}

void MainWindow::saveSettings() {
  Settings::get()->saveGeometryWidget(*this);
  Settings::get()->saveAppFontSize();
}

void MainWindow::loadSettiongs() {
  Settings::get()->loadGeometryWidget(*this);
  Settings::get()->loadAppFontSize();
}

void MainWindow::appFontSizeUpdate(int size) {
  QFont tmpFont = ui->pushButton_forcedStop->font();
  tmpFont.setPointSize(size + 2);
  ui->pushButton_forcedStop->setFont(tmpFont);

  tmpFont = ui->statusBar->font();
  tmpFont.setPointSize(size);
  ui->statusBar->setFont(tmpFont);
}

void MainWindow::setDisableActionUi(bool state) {
  static bool p_state = false;
  if (p_state != state)
    p_state = state;
  else
    return;

  for (auto *a : ui->menuInstruments->actions()) a->setEnabled(!state);

  for (int i = 0; i < 2; i++)
    ui->menuOther->actions().at(i)->setEnabled(!state);

  QStackedWidget *stack = ui->stackedWidget_actions;
  if (state) {
    stack->setCurrentIndex(1);
  } else {
    stack->setCurrentIndex(0);
  }
}

void MainWindow::setActionButton(ButtonActionState state) {
  m_buttonAction = state;
  switch (state) {
    case ButtonActionState::update:
      ui->pushButton_actions->setText(
          trUtf8("Обновить рабочую копию (update)"));
      break;
    case ButtonActionState::checkout:
      ui->pushButton_actions->setText(
          trUtf8("Извлечь рабочую копию (checkout)"));
      break;
    case ButtonActionState::upgrade:
      ui->pushButton_actions->setText(
          trUtf8("Обновить формат рабочей копии (upgrade)"));
      break;
    case ButtonActionState::clenup:
      ui->pushButton_actions->setText(
          trUtf8("Очистить рабочую копию (cleanup)"));
      break;
    case ButtonActionState::revert:
      ui->pushButton_actions->setText(
          trUtf8("Отменить изменения рабочей копии (revert)"));
      break;
    case ButtonActionState::undefined:
      ui->pushButton_actions->setText(trUtf8("Действие не определено"));
      break;
  }
}

void MainWindow::resetResultLabels() {
  const QString &wait = trUtf8("ожидаем");
  ui->label_workingCopyRevResult->setText(wait);
  ui->label_repsitoryRevResult->setText(wait);
  ui->label_networkResult->setText(wait);
  ui->label_svnResult->setText(wait);
}

void MainWindow::runNetworkTest() {
  LOG(INFO) << QString(trUtf8("Выбран репозиторий: %1"))
                   .arg(Settings::get()->getSvnManager().getUrl());
  m_networkTest->testNetwork();
  m_networkTest->testSvnHost();
}

void MainWindow::updateSvnInfo() {
  m_svnUtil->getInfoWorkingCopy();
  m_svnUtil->getInfoRepository();
}

void MainWindow::receivStateNetwork(bool state) {
  if (state)
    ui->label_networkResult->setText(trUtf8("доступен"));
  else
    ui->label_networkResult->setText(trUtf8("недоступен"));
}

void MainWindow::receivStateSvnHost(bool state) {
  if (state)
    ui->label_svnResult->setText(trUtf8("доступен"));
  else
    ui->label_svnResult->setText(trUtf8("недоступен"));
}

void MainWindow::receivRequiredAction(TypesAction type) {
  switch (type) {
    case TypesAction::upgrade:
      ui->label_workingCopyRevResult->setText(trUtf8("устаревший формат"));
      setActionButton(ButtonActionState::upgrade);
      break;
    case TypesAction::checkout:
      ui->label_workingCopyRevResult->setText(trUtf8("информация отсутствует"));
      setActionButton(ButtonActionState::checkout);
      break;
    case TypesAction::cleanup:
      ui->label_workingCopyRevResult->setText(trUtf8("требуется очистка"));
      setActionButton(ButtonActionState::clenup);
      break;
    case TypesAction::revert:
      ui->label_workingCopyRevResult->setText(
          trUtf8("требуется отмена изменений"));
      setActionButton(ButtonActionState::revert);
      break;
    case TypesAction::update:
      ui->label_workingCopyRevResult->setText(trUtf8("требуется обновление"));
      setActionButton(ButtonActionState::update);
      break;
    case TypesAction::success:
      setActionButton(ButtonActionState::update);
      break;
    case TypesAction::undefined:
      setActionButton(ButtonActionState::undefined);
      break;
    default:
      setActionButton(ButtonActionState::undefined);
      break;
  }
}

void MainWindow::receivWorkingCopyRev(qint32 rev) {
  if (rev >= 0) {
    LOG(INFO) << trUtf8("Ревизия рабочей копии: %1").arg(rev);
    ui->label_workingCopyRevResult->setText(QString::number(rev));
    setActionButton(ButtonActionState::update);
  } else {
    LOG(WARNING) << trUtf8("Ревизия рабочей копии не получена.");
  }
}

void MainWindow::receivRepositoryRev(qint32 rev) {
  if (rev >= 0) {
    LOG(INFO) << trUtf8("Ревизия репозитория: %1").arg(rev);
    ui->label_repsitoryRevResult->setText(QString::number(rev));
  } else {
    LOG(WARNING) << trUtf8("Ревизия репозитория не получена.");
    ui->label_repsitoryRevResult->setText(trUtf8("информация недоступна"));
  }
}

void MainWindow::receivStateChanged(QProcess::ProcessState state) {
  Q_UNUSED(state);
  switch (m_svnUtil->stateRunningProcess()) {
    case QProcess::NotRunning:
      setDisableActionUi(false);
      break;
    case QProcess::Starting:
      break;
    case QProcess::Running:
      setDisableActionUi(true);
      break;
  }
}

void MainWindow::updateInfo() {
  resetResultLabels();
  runNetworkTest();
  updateSvnInfo();
}

void MainWindow::svnSelected() {
  runNetworkTest();
  m_svnUtil->forcedStop();
  m_svnUtil->relocate();

  const QString &wait = trUtf8("ожидаем");
  ui->label_repsitoryRevResult->setText(wait);
  m_svnUtil->getInfoRepository();
}

void MainWindow::closeEvent(QCloseEvent *event) {
  if (m_svnUtil->stateRunningProcess() == QProcess::Running) {
    event->ignore();
    LOG(WARNING) << trUtf8("Дождитесь окончания выполняемых операции!");
  }
  saveSettings();
}

void MainWindow::on_pushButton_updateInfo_clicked() { updateInfo(); }

void MainWindow::on_pushButton_clearLog_clicked() { ui->textEdit_log->clear(); }

void MainWindow::on_pushButton_actions_clicked() {
  const QString &wait = trUtf8("ожидаем");
  ui->label_workingCopyRevResult->setText(wait);

  switch (m_buttonAction) {
    case ButtonActionState::update:
      m_svnUtil->executeAction(TypesAction::update);
      break;
    case ButtonActionState::upgrade:
      m_svnUtil->executeAction(TypesAction::upgrade);
      break;
    case ButtonActionState::checkout:
      m_svnUtil->executeAction(TypesAction::checkout);
      break;
    case ButtonActionState::clenup:
      m_svnUtil->executeAction(TypesAction::cleanup);
      break;
    case ButtonActionState::revert:
      m_svnUtil->executeAction(TypesAction::revert);
      break;
    default:
      break;
  }
}

void MainWindow::on_pushButton_copyLog_clicked() {
  QClipboard *clipboard = QApplication::clipboard();
  clipboard->setText(ui->textEdit_log->toPlainText());
}

void MainWindow::on_pushButton_forcedStop_clicked() { m_svnUtil->forcedStop(); }

void MainWindow::on_action_relocate_triggered() { m_svnUtil->relocate(); }

void MainWindow::on_action_revert_triggered() { m_svnUtil->revert(); }

void MainWindow::on_action_deleteSvnDB_triggered() {
  int reply = QMessageBox::question(
      this, trUtf8("Подтверждение действий"),
      trUtf8("Вы действиетельно хотите удалить базу данных SVN рабочей копии?"),
      QMessageBox::Yes | QMessageBox::No, QMessageBox::No);
  if (reply == QMessageBox::Yes) m_svnUtil->deleteSvnDB();
}

void MainWindow::on_action_makeAndBuild_triggered() {
  DialogMakeAndBuild makeAndBuild;
  makeAndBuild.exec();
}

void MainWindow::on_action_qt_triggered() { QMessageBox::aboutQt(this); }

void MainWindow::on_action_about_triggered() {
  QString autor = trUtf8("Автор: %1").arg("CriDos");
  QString mail = trUtf8("Почта: %1").arg("CriDos@ya.ru");
  QString version = trUtf8("Версия приложения: %1\n").arg(APP_VERSION);
  QString message = QString("%1\n%2\n%3").arg(autor).arg(mail).arg(version);

  QMessageBox::about(this, trUtf8("Информация"), message);
}

void MainWindow::on_action_svnManager_triggered() {
  DialogSvnManager sm;
  connect(&sm, &DialogSvnManager::onSelected, this, &MainWindow::svnSelected);
  sm.exec();
}

void MainWindow::on_action_update_triggered() { m_autoupdate->checkUpdate(); }

void MainWindow::on_action_changelog_triggered() {
  DialogChangelog cl;
  cl.exec();
}

void MainWindow::on_action_bugreport_triggered() {
  QDesktopServices::openUrl(
      QUrl("http://forum.hiasm.net/forum.html?q=3&p=210696"));
}

void MainWindow::on_action_settings_triggered() {
  DialogSettings dialogSettings;
  dialogSettings.exec();
}
