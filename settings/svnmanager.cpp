// Project
#include "svnmanager.h"
#include "settings.h"

// LOG
#include "logger.h"

// STL

// Native

// Qt
#include <QFileInfo>
#include <QUrl>

SvnManager::SvnManager(QSettings &settings, QObject *parent)
    : QObject(parent), m_settings(settings) {}

const QStringList SvnManager::getUrlList() const {
  QVariant value = m_settings.value(Keys::SVNManager::URL_LIST);
  if (value.isNull())
    return m_defaultSvnList;
  else {
    QStringList list = value.toStringList();

    if (list.isEmpty())
      return m_defaultSvnList;
    else
      return list;
  }
}

void SvnManager::saveListSvn(QStringList list) {
  m_settings.setValue(Keys::SVNManager::URL_LIST, list);
}

const QString SvnManager::getUrl() const {
  const QVariant val = m_settings.value(Keys::SVNManager::CURRENT_URL);
  if (!val.isNull()) {
    return val.toString();
  }

  return getUrlList().first();
}

const QString SvnManager::getHost() const {
  QUrl url = getUrl();
  return url.host();
}

void SvnManager::resetSvnList() {
  m_settings.setValue(Keys::SVNManager::URL_LIST, m_defaultSvnList);
}

void SvnManager::setUrl(const QString &url) {
  m_settings.setValue(Keys::SVNManager::CURRENT_URL, url);
}
