// Project
#include "dialogsvnmanager.h"
#include "logger.h"
#include "ui_dialogsvnmanager.h"

// STL

// Native

// Qt
#include <QInputDialog>
#include <QListWidget>
#include <QMessageBox>
#include <QUrl>

DialogSvnManager::DialogSvnManager(QWidget *parent)
    : QDialog(parent),
      m_svnManager(Settings::get()->getSvnManager()),
      ui(new Ui::DialogSvnManager) {
  ui->setupUi(this);
  setWindowFlags(windowFlags() & ~Qt::WindowContextHelpButtonHint);

  loadListSvn();
  setLabelCurrentUrl();
  updateSize();
}

DialogSvnManager::~DialogSvnManager() { delete ui; }

void DialogSvnManager::updateSize() {
  setFixedSize(sizeHint());
  ui->gridLayout->setSizeConstraint(QLayout::SetFixedSize);
}

void DialogSvnManager::loadListSvn() {
  ui->listWidgetSvn->clear();
  for (const QString &s : m_svnManager.getUrlList()) {
    QListWidgetItem *item = new QListWidgetItem(s, ui->listWidgetSvn);
    item->setTextAlignment(Qt::AlignHCenter);
  }
}

void DialogSvnManager::saveListSvn() {
  QStringList items;
  for (int i = 0; i < ui->listWidgetSvn->count(); ++i) {
    QListWidgetItem *item = ui->listWidgetSvn->item(i);
    items.append(item->text());
  }

  m_svnManager.saveListSvn(items);
}

void DialogSvnManager::addUrl(const QString &url) {
  ui->listWidgetSvn->addItem(url);
  saveListSvn();
}

void DialogSvnManager::setLabelCurrentUrl() {
  ui->label_currentUrl->setText(m_svnManager.getUrl());
}

void DialogSvnManager::on_pushButton_add_clicked() {
  bool ok = false;
  QString reply = QInputDialog::getText(
      this, trUtf8("Ввод URL-адреса"), trUtf8("Введите URL-адрес репозитория"),
      QLineEdit::Normal, "http://", &ok,
      Qt::WindowSystemMenuHint | Qt::WindowTitleHint);
  if (!ok) return;

  QUrl url = QUrl::fromUserInput(reply);

  if (!url.isValid()) {
    QMessageBox::warning(
        this, trUtf8("Предупреждение"),
        trUtf8("Введён некорректный URL-адрес, попробуйте ещё раз!"));
  } else {
    QListWidgetItem *item =
        new QListWidgetItem(url.toString(), ui->listWidgetSvn);
    ui->listWidgetSvn->setCurrentItem(item);
    item->setTextAlignment(Qt::AlignCenter);
    saveListSvn();
  }
}

void DialogSvnManager::on_pushButton_del_clicked() {
  auto item = ui->listWidgetSvn->currentItem();
  if (item == nullptr) {
    QMessageBox::warning(this, trUtf8("Предупреждение"),
                         trUtf8("Вы ничего не выбрали!"));
    return;
  }

  int reply = QMessageBox::question(
      this, trUtf8("Подтверждение действий"), trUtf8("Действительно удалить?"),
      QMessageBox::No | QMessageBox::Yes, QMessageBox::No);

  if (reply == QMessageBox::No) return;

  delete item;
  saveListSvn();
}

void DialogSvnManager::on_pushButton_def_clicked() {
  m_svnManager.resetSvnList();
  loadListSvn();
}

void DialogSvnManager::on_pushButton_select_clicked() {
  auto item = ui->listWidgetSvn->currentItem();
  if (item == nullptr) {
    QMessageBox::warning(this, trUtf8("Предупреждение"),
                         trUtf8("Вы ничего не выбрали!"));
    return;
  }

  updateSize();
  m_svnManager.setUrl(item->text());
  setLabelCurrentUrl();
  emit onSelected();
}

void DialogSvnManager::on_pushButton_edit_clicked() {
  auto item = ui->listWidgetSvn->currentItem();
  if (item == nullptr) {
    QMessageBox::warning(this, trUtf8("Предупреждение"),
                         trUtf8("Вы ничего не выбрали!"));
    return;
  }

  bool ok = false;
  QString reply = QInputDialog::getText(
      this, trUtf8("Изменение URL-адреса"),
      trUtf8("Внесите изменения или выберите отмену"), QLineEdit::Normal,
      item->text(), &ok, Qt::WindowSystemMenuHint | Qt::WindowTitleHint);
  if (!ok) return;

  QUrl url = QUrl::fromUserInput(reply);
  if (!url.isValid()) {
    QMessageBox::warning(
        this, trUtf8("Предупреждение"),
        trUtf8("Введён некорректный URL-адрес, попробуйте ещё раз!"));
  } else {
    item->setText(url.toString());
    saveListSvn();
  }
}
