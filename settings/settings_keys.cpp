#include "settings_keys.h"

namespace Keys {
namespace App {
const QString FONT_SIZE = "APP/FONT_SIZE";
}

namespace Ui {
const QString WINDOW_GEOMETRY = "UI/WINDOW_GEOMETRY";
}

namespace SVNManager {
const QString URL_LIST = "SVNManager/URL_LIST";
const QString CURRENT_URL = "SVNManager/CURRENT_URL";
const QString PATH_PACKAGES = "SVNManager/PATH_PACKAGES";
}
namespace MakeAndBuild {
const QString EXCLUDE_DIRS = "MakeAndBuild/EXCLUDE_DIRS";
const QString FIND_BAT_FILES = "MakeAndBuild/FIND_BAT_FILES";
const QString EXCLUDE_PACK_DIRS = "MakeAndBuild/EXCLUDE_PACK_DIRS";
}
namespace Autoupdate {
const QString ENABLE_AUTOUPDATE = "Autoupdate/ENABLE_AUTOUPDATE";
}
}
