// Project
#include "settings.h"

// LOG

// STL

// Native

// Qt
#include <QApplication>
#include <QDebug>
#include <QFileInfo>
#include <QFont>

Q_GLOBAL_STATIC(Settings, SettingsInstance)

Settings::Settings() : m_svnManager(m_settings) {}

Settings *Settings::get() { return SettingsInstance(); }

int Settings::getAppFontSize() {
  QFont tmpFont = QApplication::font();
  return tmpFont.pointSize();
}

void Settings::saveGeometryWidget(const QWidget &widget) {
  m_settings.beginGroup(widget.objectName());
  m_settings.setValue(Keys::Ui::WINDOW_GEOMETRY, widget.geometry());
  m_settings.endGroup();
}

void Settings::loadGeometryWidget(QWidget &widget) {
  m_settings.beginGroup(widget.objectName());
  QVariant newGeometry = m_settings.value(Keys::Ui::WINDOW_GEOMETRY);
  if (!newGeometry.isNull()) {
    widget.setGeometry(newGeometry.toRect());
  }
  m_settings.endGroup();
}

void Settings::saveAppFontSize() {
  m_settings.setValue(Keys::App::FONT_SIZE, QApplication::font().pointSize());
}

void Settings::saveAppFontSize(int size) {
  m_settings.setValue(Keys::App::FONT_SIZE, size);
  QFont tmpFont = QApplication::font();
  tmpFont.setPointSize(size);
  QApplication::setFont(tmpFont);

  onAppFontSizeUpdate(size);
}

void Settings::loadAppFontSize() {
  QVariant newAppFontSize = m_settings.value(Keys::App::FONT_SIZE);
  if (!newAppFontSize.isNull()) {
    int newSize = newAppFontSize.toInt();

    QFont tmpFont = QApplication::font();
    tmpFont.setPointSize(newSize);
    QApplication::setFont(tmpFont);

    onAppFontSizeUpdate(newSize);
  }
}

SvnManager &Settings::getSvnManager() { return m_svnManager; }

void Settings::setStateAutoupdate(bool state) {
  m_settings.setValue(Keys::Autoupdate::ENABLE_AUTOUPDATE, state);
  emit onChangeStateAutoupdate(state);
}

bool Settings::getStateAutoupdate() {
  auto state = m_settings.value(Keys::Autoupdate::ENABLE_AUTOUPDATE);
  if (state.isNull())
    return true;
  else
    return state.toBool();
}

QString Settings::getPathPackages() const {
  auto pathPackages = m_settings.value(Keys::SVNManager::PATH_PACKAGES);
  if (pathPackages.isNull())
    return m_defaultPackagesDir;
  else
    return pathPackages.toString();
}

void Settings::setPathPackages(const QString &pathPackages) {
  m_settings.setValue(Keys::SVNManager::PATH_PACKAGES, pathPackages);
  emit onChangePathPackages(pathPackages);
}

void Settings::resetPathPackages() {
  m_settings.setValue(Keys::SVNManager::PATH_PACKAGES, m_defaultPackagesDir);
  emit onChangePathPackages(m_defaultPackagesDir);
}

QStringList Settings::getExcludeDirs() {
  auto excludeDirs = m_settings.value(Keys::MakeAndBuild::EXCLUDE_DIRS);
  if (excludeDirs.isNull())
    return m_defaultExcludeDirs;
  else
    return excludeDirs.toStringList();
}

void Settings::setExcludeDirs(QStringList list) {
  m_settings.setValue(Keys::MakeAndBuild::EXCLUDE_DIRS, list);
  emit onChangeExcludeDirs(list);
}

void Settings::resetExcludeDirs() {
  m_settings.setValue(Keys::MakeAndBuild::EXCLUDE_DIRS, m_defaultExcludeDirs);
  emit onChangeExcludeDirs(m_defaultExcludeDirs);
}

QStringList Settings::getFindBatFiles() {
  auto findBatFiles = m_settings.value(Keys::MakeAndBuild::FIND_BAT_FILES);
  if (findBatFiles.isNull())
    return m_defaultFindBatFiles;
  else
    return findBatFiles.toStringList();
}

void Settings::setFindBatFiles(QStringList list) {
  m_settings.setValue(Keys::MakeAndBuild::FIND_BAT_FILES, list);
  emit onChangeFindBatFiles(list);
}

void Settings::resetFindBatFiles() {
  m_settings.setValue(Keys::MakeAndBuild::FIND_BAT_FILES,
                      m_defaultFindBatFiles);
  emit onChangeFindBatFiles(m_defaultFindBatFiles);
}

QStringList Settings::getExcludePackDirs() {
  auto excludePackDirs =
      m_settings.value(Keys::MakeAndBuild::EXCLUDE_PACK_DIRS);
  if (excludePackDirs.isNull())
    return m_defaultExcludePackDirs;
  else
    return excludePackDirs.toStringList();
}

void Settings::setExcludePackDirs(QStringList list) {
  m_settings.setValue(Keys::MakeAndBuild::EXCLUDE_PACK_DIRS, list);
  emit onChangeExcludePackDirs(list);
}

void Settings::resetExcludePackDirs() {
  m_settings.setValue(Keys::MakeAndBuild::EXCLUDE_PACK_DIRS,
                      m_defaultExcludePackDirs);
  emit onChangeExcludePackDirs(m_defaultExcludePackDirs);
}
