#pragma once

// Project
#include <QSettings>

// STL

// Native

// Qt
#include <QObject>

class SvnManager : public QObject {
  Q_OBJECT

 private:
  const QStringList m_defaultSvnList = {"http://svn.hiasm.com/packs",
                                        "http://svn.hiasm.net/packs"};

 private:
  QSettings &m_settings;

 public:
  explicit SvnManager(QSettings &settings, QObject *parent = 0);

 public:
  const QStringList getUrlList() const;
  void saveListSvn(QStringList list);
  const QString getUrl() const;
  const QString getHost() const;
  void resetSvnList();
  void setUrl(const QString &url);
};
