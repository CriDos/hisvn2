#pragma once

// Project

// STL

// Native

// Qt
#include <QDialog>

namespace Ui {
class DialogSettings;
}

class DialogSettings : public QDialog {
  Q_OBJECT

 public:
  explicit DialogSettings(QWidget *parent = 0);
  ~DialogSettings();

 private slots:
  void on_toolButton_resetPathPackages_clicked();
  void on_toolButton_selectPathPackages_clicked();

 private:
  Ui::DialogSettings *ui;
};
